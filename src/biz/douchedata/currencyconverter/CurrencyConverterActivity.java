package biz.douchedata.currencyconverter;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Currency;
import java.util.Date;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

@TargetApi(8)
public class CurrencyConverterActivity extends Activity {

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		Log.i("debug", "starting app");
		running = true;

		// Figure out whether we are online
		ConnectivityManager cm = (ConnectivityManager) this
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		online = (cm.getActiveNetworkInfo() != null);
		Log.i("debug", "is online: " + online);

		// Set up UI elements
		spinner1 = (Spinner) findViewById(R.id.spinner1);
		spinner2 = (Spinner) findViewById(R.id.spinner2);
		input1 = (EditText) findViewById(R.id.editText1);
		input2 = (EditText) findViewById(R.id.editText2);
		message = (TextView) findViewById(R.id.textView3);
		progress = (ProgressBar) findViewById(R.id.progressBar1);

		// Instantiate model class
		model = new CurrencyConverterModel(xmlFile, url, this);
		Log.i("debug", "XML file exists: " + model.file.exists());

		// Some data
		model.currency.add(Currency.getInstance("EUR"));
		model.code.add("EUR");
		model.rate.add(new BigDecimal(1.00));

		// Asynchronously fetch XML and update UI
		new Update().execute(model);
	}

	public void onDestroy() {
		super.onDestroy();
		running = false;
	}

	/* Perform exchange and update corresponding EditText */
	private void updateExchangeView(EditText in, Spinner from, Spinner to,
			EditText out) {
		BigDecimal sum = null;
		if (!model.file.exists() || in.getText().length() == 0)
			out.setText("");
		else {
			try {
				sum = new BigDecimal(in.getText().toString());
			} catch (NumberFormatException e) {
				out.setText("Invalid input");
				sum = new BigDecimal(0);
			} finally {
				out.setText(model.exchange(from.getSelectedItem().toString(),
						to.getSelectedItem().toString(), sum));
			}
		}
	}

	/* Set up listeners */
	private final void setUpListeners() {
		ad = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, model.name);

		oisl = new OnItemSelectedListenerAdapter() {
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				updateExchangeView(input1, spinner1, spinner2, input2);
			}
		};

		tw = new TextWatcherAdapter() {
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				updateExchangeView(input1, spinner1, spinner2, input2);
			}
		};

		spinner1.setAdapter(ad);
		spinner2.setAdapter(ad);
		spinner1.setOnItemSelectedListener(oisl);
		spinner2.setOnItemSelectedListener(oisl);
		input1.addTextChangedListener(tw);

	}

	/* Asynchronous task to download XML resource, parse it and update UI */
	private class Update extends
			AsyncTask<CurrencyConverterModel, Integer, Boolean> {

		@Override
		protected Boolean doInBackground(CurrencyConverterModel... model) {
			boolean problem = false;
			for (CurrencyConverterModel m : model) {
				while (running) {
					if ((m.timestamp == null || !m.timestamp.equals(today))
							&& online) {
						Log.i("debug", "downloaded " + m.download() + " bytes");
					}
					m.load();
					m.stamp(today);
					if (!m.file.exists() && online && !problem)
						problem = true;
					break;
				}
			}
			return problem;
		}

		/* Hide ProgressBar and enable program */
		protected void onPostExecute(Boolean problem) {
			progress.setVisibility(View.GONE);

			if (!problem) {

				if (model.timestamp == null)
					message.setText("Welcome. Rates courtesy of European "
							+ "Central Bank");
				else if (model.timestamp.equals(today))
					message.setText("Rates retrieved today");
				else
					message.setText("Rates retrieved from ECB: "
							+ model.timestamp);
				if (!model.file.exists() && !online)
					message.setText("Application needs to be run online"
							+ " atleast once");

				setUpListeners();
			} else {
				spinner1.setVisibility(View.GONE);
				spinner2.setVisibility(View.GONE);
				input1.setVisibility(View.GONE);
				input2.setVisibility(View.GONE);

				message.setText("Uh oh. " + "Something went horribly wrong.");
			}
		}
	}

	Spinner spinner1, spinner2;
	EditText input1, input2;
	ProgressBar progress;
	TextView message;
	CurrencyConverterModel model;
	OnItemSelectedListener oisl;
	TextWatcher tw;
	View.OnClickListener ocl;
	ArrayAdapter<String> ad;
	boolean running, online;
	final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
	final String today = sdf.format(new Date()), xmlFile = "eurofxref-daily.xml",
			url = "http://www.ecb.europa.eu/stats/eurofxref/" + xmlFile;
}
