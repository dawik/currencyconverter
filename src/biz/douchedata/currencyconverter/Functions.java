package biz.douchedata.currencyconverter;

import java.io.IOException;
import java.io.InputStream;

public final class Functions {

	private Functions() {
	}

	public static CharSequence read(InputStream is) {
		StringBuilder text = new StringBuilder();

		int numRead = 0;
		try {
			while ((numRead = is.read(buffer)) >= 0) {
				text.append(new String(buffer, 0, numRead));
			}
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return text;
	}

	private static byte[] buffer = new byte[1024];
}