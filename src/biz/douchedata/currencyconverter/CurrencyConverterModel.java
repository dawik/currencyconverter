package biz.douchedata.currencyconverter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.ContextWrapper;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class CurrencyConverterModel {

	/* Super duper mega model */
	public CurrencyConverterModel(String filename, String url, Activity action) {
		
		cw = new ContextWrapper(action);
		prefs = PreferenceManager.getDefaultSharedPreferences(action);
		timestamp = prefs.getString("timestamp", null);
		
		this.edit = prefs.edit();
		this.url = url;
		
		file = new File(cw.getFilesDir(), "/" + filename);
		
		if (!file.exists())
			file.getParentFile().mkdirs();

		try {
			res = new ResourceInformation(Functions.read(cw.getResources().getAssets()
					.open(assetFile)));
		} catch (IOException e) {
			Log.i("error","Can't access " + assetFile);
		}


	}

	/*	Read, parse and store resource information I.E long currency names in subclass */
	public class ResourceInformation {
		ResourceInformation(CharSequence data) {
			pattern = Pattern.compile("\"([^\"]+)\".*?:.*?\"([^\"]+)\"");
			matcher = pattern.matcher(data);
			while (matcher.find()) {
				code.add(matcher.group(1));
				name.add(matcher.group(2));
			}
		}

		public ArrayList<String> code = new ArrayList<String>();
		public ArrayList<String> name = new ArrayList<String>();
	}

	/* Set timestamp */
	public void stamp(String ts) {
		edit.putString("timestamp", ts);
		edit.commit();
	}
	
	/* Attempt to download file */
	public final long download() {

		if (file.exists())
			file.delete();

		int numBytes = 0, bytesRead = 0;

		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet request = new HttpGet(url);
			HttpResponse response = httpClient.execute(request);
			
			fos = new FileOutputStream(file);
			is = response.getEntity().getContent();

			while ((numBytes = is.read(buffer)) > 0) {
				fos.write(buffer, 0, numBytes);
				bytesRead += numBytes;
			}

			fos.close();
			is.close();
		} catch (Exception e) {
			file.delete();
		}

		return bytesRead;
	}

	/* Parse XML file, store currency information, rates and extend names by associating
	 * with asset file.
	 */
	public final void load() {
		if (file.exists()) {
			pattern = Pattern.compile("currency=\'([^\']+)\'.*?rate=\'([^\']+)\'");
			try {
				matcher = pattern.matcher(Functions.read(new FileInputStream(file)));
			} catch (FileNotFoundException e) {
				Log.i("error", "Not found: " + file.getAbsoluteFile());
			}

			while (matcher.find()) {
				currency.add(Currency.getInstance(matcher.group(1)));
				code.add(matcher.group(1));
				rate.add(new BigDecimal(matcher.group(2)));
			}

			for (String i : code) {
				name.add(res.name.get(res.code.indexOf(i)) + " [" + i + "]");
			}
		}
	}

	/* Exchange function */
	public final String exchange(String from, String to, BigDecimal sum) {
		nf.setCurrency(currency.get(name.indexOf(to)));
		nf.setMaximumFractionDigits(2);
		return nf.format(sum.divide(rate.get(name.indexOf(from)), 2,
				RoundingMode.HALF_DOWN).multiply(rate.get(name.indexOf(to))))
				+ " " + currency.get(name.indexOf(to)).getSymbol();
	}

	public ArrayList<String> name = new ArrayList<String>();
	public ArrayList<String> code = new ArrayList<String>();
	public ArrayList<Currency> currency = new ArrayList<Currency>();
	public ArrayList<BigDecimal> rate = new ArrayList<BigDecimal>();
	public ResourceInformation res = null;
	private InputStream is = null;
	private FileOutputStream fos = null;
	private byte[] buffer = new byte[1024];
	private NumberFormat nf = NumberFormat.getInstance();
	private ContextWrapper cw;
	public SharedPreferences prefs;
	public SharedPreferences.Editor edit;
	public File file = null;
	protected Matcher matcher = null;
	protected Pattern pattern = null;
	public String timestamp = null, url = null;
	public final String assetFile = "iso4217.json";
}
